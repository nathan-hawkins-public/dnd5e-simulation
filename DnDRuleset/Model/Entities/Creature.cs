namespace DnDRuleset.Model.Entities
{
	using System.Collections.Generic;

	using DnDRuleset.Model.Enums;

	using Size = System.Drawing.Size;

	public class Creature
	{
		public string Name { get; }
		public Size Size { get; }
		public CreatureType CreatureType { get; }
		public Alignment Alignment { get; }
		public List<CreatureTag> Tags { get; }
		public int ArmorClass { get; set; }
		
		
	}
}
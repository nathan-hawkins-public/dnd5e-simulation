namespace DnDRuleset.Model.Services.Parsers
{
	using System.Collections.Generic;
	using System.Linq;

	public class DiceExpressionParser : ExpressionParser
	{
		private const string RegexMatchesOptionalHyphenDigitsSeparatorDigits = @"^[-]?\d+[d]\d+$";

		public override string[] Parse(string input)
		{
			IEnumerable<string> expressions = SplitToExpressions(input);
			string[] rawDiceExpressions = expressions.Where(IsDiceExpression).ToArray();
			string[] parsedDiceExpressions = ParseDiceExpressions(rawDiceExpressions);

			VerifyExpressionsMatchPattern(input, parsedDiceExpressions, RegexMatchesOptionalHyphenDigitsSeparatorDigits);

			return parsedDiceExpressions;
		}

		private static string[] ParseDiceExpressions(IEnumerable<string> diceExpressions)
		{
			string[] parsedExpressions = diceExpressions.Select(expression =>
				{
					int indexOfSeparator = expression.IndexOf(DiceExpressionSeparator);

					if (indexOfSeparator == 0)
					{
						expression = "1" + expression;
					} else if (expression[indexOfSeparator - 1].Equals('-'))
					{
						expression = expression.Insert(1, "1");
					}

					return expression;
				}).ToArray();

			return parsedExpressions;
		}
	}
}
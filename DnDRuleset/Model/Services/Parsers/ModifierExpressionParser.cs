namespace DnDRuleset.Model.Services.Parsers
{
	using System.Collections.Generic;
	using System.Linq;

	public class ModifierExpressionParser : ExpressionParser
	{
		private const string RegexMatchesOptionalHyphenDigits = @"^[-]?\d+$";

		public override string[] Parse(string input)
		{
			IEnumerable<string> expressions = SplitToExpressions(input);
			string[] nonDiceExpressions = expressions.Where(expression => !IsDiceExpression(expression)).ToArray();
			string[] filteredModifierExpressions = FilterEmptyExpressions(nonDiceExpressions);

			VerifyExpressionsMatchPattern(input, filteredModifierExpressions, RegexMatchesOptionalHyphenDigits);

			return filteredModifierExpressions;
		}

		private static string[] FilterEmptyExpressions(IEnumerable<string> expressions)
		{
			return expressions.Where(expression => !string.IsNullOrEmpty(expression)).ToArray();
		}
	}
}
namespace DnDRuleset.Model.Services.Parsers
{
	using System.Collections.Generic;
	using System.Linq;
	using System.Text.RegularExpressions;

	using DnDRuleset.Model.Exceptions;

	public abstract class ExpressionParser : IParser
	{
		protected const char DiceExpressionSeparator = 'd';
		private const string RegexSplitByAndIncludeAtBeginningPlusOrHyphen = @"(?=[+-])";
		private const string UnableToParseRollInput = "Unable to parse roll input: ";

		public abstract string[] Parse(string input);

		protected static IEnumerable<string> SplitToExpressions(string input)
		{
			return Regex.Split(input, RegexSplitByAndIncludeAtBeginningPlusOrHyphen)
				.Select(expression => expression.Replace(" ", "").Replace("+", ""))
				.ToArray();
		}

		protected static bool IsDiceExpression(string expression)
		{
			return expression.Contains(DiceExpressionSeparator);
		}

		protected static void VerifyExpressionsMatchPattern(string input, IEnumerable<string> expressions, string regexPattern)
		{
			if (expressions.Any(expression => !Regex.IsMatch(expression, regexPattern)))
			{
				throw new RollParsingException(UnableToParseRollInput + input);
			}
		}
	}
}
namespace DnDRuleset.Model.Services.Parsers
{
	public interface IParser
	{
		string[] Parse(string input);
	}
}
namespace DnDRuleset.Model.Services
{
	using System;

	public class RandomNumberGenerator
	{
		private readonly Random random = new Random();

		public virtual int Next(int maxValue)
		{
			return random.Next(maxValue);
		}
	}
}
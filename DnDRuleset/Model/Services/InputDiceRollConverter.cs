namespace DnDRuleset.Model.Services
{
	using System.Collections.Generic;
	using System.Linq;

	using DnDRuleset.Model.Domain;
	using DnDRuleset.Model.Services.Parsers;

	public class InputDiceRollConverter
	{
		private const char DiceExpressionSeparator = 'd';

		private readonly IParser diceExpressionParser;
		private readonly IParser modifierExpressionParser;

		public InputDiceRollConverter(IParser diceExpressionParser, IParser modifierExpressionParser)
		{
			this.modifierExpressionParser = modifierExpressionParser;
			this.diceExpressionParser = diceExpressionParser;
		}

		public DiceRoll ConvertInputToDiceRoll(string input)
		{
			string[] parsedDiceExpressions = diceExpressionParser.Parse(input);
			string[] parsedModifierExpressions = modifierExpressionParser.Parse(input);

			IEnumerable<(int, int)> convertedDiceExpressions = ConvertDiceExpressionsFrom(parsedDiceExpressions);
			IEnumerable<int> convertedModifiers = parsedModifierExpressions.Select(int.Parse).ToArray();

			return new DiceRoll(convertedDiceExpressions, convertedModifiers);
		}

		private static IEnumerable<(int, int)> ConvertDiceExpressionsFrom(IEnumerable<string> diceExpressions)
		{
			return diceExpressions.Select(ConvertDiceExpression).ToArray();
		}

		private static (int, int) ConvertDiceExpression(string expression)
		{
			string[] components = expression.Split(DiceExpressionSeparator);

			int dieAmount = int.Parse(components[0]);
			int dieSize = int.Parse(components[1]);

			return (dieAmount, dieSize);
		}
	}
}
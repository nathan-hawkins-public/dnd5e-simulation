namespace DnDRuleset.Model.Domain
{
	using System;
	using System.Collections.Generic;

	using DnDRuleset.Model.Services;

	public class DiceRoll
	{
		private readonly RandomNumberGenerator randomNumberGenerator;

		public List<(int diceAmount, int dieSize)> DiceExpressions { get; }
		public List<int> ModifierExpressions { get; }

		public DiceRoll(RandomNumberGenerator randomNumberGenerator, IEnumerable<(int, int)> diceExpressions, IEnumerable<int> modifierExpressions)
		{
			DiceExpressions = new List<(int diceAmount, int dieSize)>(diceExpressions);
			ModifierExpressions = new List<int>(modifierExpressions);
			this.randomNumberGenerator = randomNumberGenerator;
		}

		public DiceRoll(IEnumerable<(int, int)> diceExpressions) : this(new RandomNumberGenerator(), diceExpressions, new List<int>())
		{}

		public DiceRoll(IEnumerable<int> modifierExpressions) : this(new RandomNumberGenerator(), new List<(int, int)>(), modifierExpressions)
		{}

		public DiceRoll(IEnumerable<(int, int)> diceExpressions, IEnumerable<int> modifierExpressions) :
			this(new RandomNumberGenerator(), diceExpressions, modifierExpressions)
		{}

		public int Roll()
		{
			int total = 0;

			DiceExpressions.ForEach(diceExpression =>
				{
					(int diceAmount, int dieSize) = diceExpression;

					for (int i = 0; i < Math.Abs(diceAmount); i++)
					{
						int roll = randomNumberGenerator.Next(dieSize);
						total += diceAmount > 0 ? roll : roll * -1;
					}
				});

			ModifierExpressions.ForEach(modifier => total += modifier);

			return total;
		}
	}
}
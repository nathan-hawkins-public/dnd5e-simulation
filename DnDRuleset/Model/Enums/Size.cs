namespace DnDRuleset.Model.Enums
{
	public enum Size
	{
		Tiny,
		Small,
		Medium,
		Large,
		Huge,
		Gargantuan
	}
}
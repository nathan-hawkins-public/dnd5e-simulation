namespace DnDRuleset.Model.Enums
{
	using System.Collections.Generic;
	using System.Linq;
	using System.Windows.Documents;

	public enum Alignment
	{
		LawfulGood,
		NeutralGood,
		ChaoticGood,
		LawfulNeutral,
		TrueNeutral,
		ChaoticNeutral,
		LawfulEvil,
		NeutralEvil,
		ChaoticEvil
	}

	public static class AlignmentExtensions
	{
		private static readonly List<Alignment> LawfulAlignments = new List<Alignment> { Alignment.LawfulGood, Alignment.LawfulNeutral, Alignment.LawfulEvil };
		private static readonly List<Alignment> NeutralAlignments = new List<Alignment>
			                                                            {
				                                                            Alignment.NeutralGood, Alignment.LawfulNeutral, Alignment.TrueNeutral,
				                                                            Alignment.ChaoticNeutral, Alignment.NeutralEvil
			                                                            };

		private static readonly List<Alignment> ChaoticAlignments = new List<Alignment> { Alignment.ChaoticGood, Alignment.ChaoticNeutral, Alignment.ChaoticEvil };

		private static readonly List<Alignment> GoodAlignments = new List<Alignment> { Alignment.LawfulGood, Alignment.NeutralGood, Alignment.ChaoticGood };

		private static readonly List<Alignment> EvilAlignments = new List<Alignment> { Alignment.LawfulEvil, Alignment.NeutralEvil, Alignment.ChaoticEvil };

		public static bool IsLawful(this Alignment alignment)
		{
			return LawfulAlignments.Contains(alignment);
		}

		public static bool IsNeutral(this Alignment alignment)
		{
			return NeutralAlignments.Contains(alignment);
		}

		public static bool IsChaotic(this Alignment alignment)
		{
			return ChaoticAlignments.Contains(alignment);
		}

		public static bool IsGood(this Alignment alignment)
		{
			return GoodAlignments.Contains(alignment);
		}
		
		public static bool IsEvil(this Alignment alignment)
		{
			return EvilAlignments.Contains(alignment);
		}
	}
}
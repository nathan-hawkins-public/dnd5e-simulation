namespace DnDRuleset.Model.Exceptions
{
	using System;

	public class RollParsingException : SystemException
	{
		public RollParsingException(string? message) : base(message)
		{}

		public RollParsingException(string? message, Exception? innerException) : base(message, innerException)
		{}
	}
}
namespace DnDRulesetTests.Model.Services
{
	using System.Linq;

	using DnDRuleset.Model.Domain;
	using DnDRuleset.Model.Services;
	using DnDRuleset.Model.Services.Parsers;

	using Moq;

	using NUnit.Framework;

	[TestFixture]
	public class InputDiceRollConverterTests
	{
		private Mock<IParser> diceExpressionParser;
		private Mock<IParser> modifierExpressionParser;
		private InputDiceRollConverter inputDiceRollConverter;

		[SetUp]
		public void SetUp()
		{
			diceExpressionParser = new Mock<IParser>();
			modifierExpressionParser = new Mock<IParser>();
			inputDiceRollConverter = new InputDiceRollConverter(diceExpressionParser.Object, modifierExpressionParser.Object);

			diceExpressionParser.Setup(parser => parser.Parse(It.IsAny<string>())).Returns(new string[0]);
			modifierExpressionParser.Setup(parser => parser.Parse(It.IsAny<string>())).Returns(new string[0]);
		}

		[TestCase("1d4")]
		[TestCase("2d20")]
		public void ConvertInputToDiceRoll_CallsDiceExpressionParser(string input)
		{
			inputDiceRollConverter.ConvertInputToDiceRoll(input);

			diceExpressionParser.Verify(parser => parser.Parse(input));
		}

		[TestCase("1d4")]
		[TestCase("2d20")]
		public void ConvertInputToDiceRoll_CallsModifierExpressionParser(string input)
		{
			inputDiceRollConverter.ConvertInputToDiceRoll(input);

			modifierExpressionParser.Verify(parser => parser.Parse(input));
		}

		[Test]
		public void ConvertInputToDiceRoll_ReturnsEmptyArrayWhenDiceExpressionParserReturnsEmptyArray()
		{
			DiceRoll actual = inputDiceRollConverter.ConvertInputToDiceRoll("");

			Assert.IsEmpty(actual.DiceExpressions);
		}

		[TestCase(new[] { "1d4" }, 1)]
		[TestCase(new[] { "1d6" }, 1)]
		[TestCase(new[] { "2d4" }, 2)]
		[TestCase(new[] { "-1d4" }, -1)]
		[TestCase(new[] { "-1d6" }, -1)]
		[TestCase(new[] { "-2d4" }, -2)]
		public void ConvertInputToDiceRoll_ReturnsCorrectDiceAmountWhenDiceExpressionParserReturnsSingleExpression(string[] diceExpressions, int expectedDiceAmount)
		{
			diceExpressionParser.Setup(parser => parser.Parse(It.IsAny<string>())).Returns(diceExpressions);

			DiceRoll actual = inputDiceRollConverter.ConvertInputToDiceRoll("");
			int actualDiceAmount = GatherDiceAmountsFrom(actual)[0];

			Assert.AreEqual(expectedDiceAmount, actualDiceAmount);
		}

		[TestCase(new[] { "1d4", "1d4" }, 1, 1)]
		[TestCase(new[] { "1d6", "1d8" }, 1, 1)]
		[TestCase(new[] { "1d6", "2d8" }, 1, 2)]
		[TestCase(new[] { "1d4", "2d4" }, 1, 2)]
		[TestCase(new[] { "2d4", "1d4" }, 2, 1)]
		[TestCase(new[] { "1d4", "2d4", "3d4" }, 1, 2, 3)]
		[TestCase(new[] { "-1d4", "2d4", "3d4" }, -1, 2, 3)]
		[TestCase(new[] { "1d4", "-2d4", "3d4" }, 1, -2, 3)]
		[TestCase(new[] { "1d4", "2d4", "-3d4" }, 1, 2, -3)]
		[TestCase(new[] { "-1d4", "-2d4", "-3d4" }, -1, -2, -3)]
		public void ConvertInputToDiceRoll_ReturnsCorrectDiceAmountsWhenDiceExpressionParserReturnsMultipleExpressions(string[] diceExpressions,
			params int[] expectedDiceAmounts)
		{
			diceExpressionParser.Setup(parser => parser.Parse(It.IsAny<string>())).Returns(diceExpressions);

			DiceRoll actual = inputDiceRollConverter.ConvertInputToDiceRoll("");
			int[] actualDiceAmounts = GatherDiceAmountsFrom(actual);

			Assert.AreEqual(expectedDiceAmounts, actualDiceAmounts);
		}

		[TestCase(new[] { "1d2" }, 2)]
		[TestCase(new[] { "1d4" }, 4)]
		[TestCase(new[] { "1d6" }, 6)]
		[TestCase(new[] { "1d8" }, 8)]
		[TestCase(new[] { "1d10" }, 10)]
		[TestCase(new[] { "1d12" }, 12)]
		[TestCase(new[] { "1d20" }, 20)]
		[TestCase(new[] { "1d100" }, 100)]
		[TestCase(new[] { "-1d4" }, 4)]
		[TestCase(new[] { "-1d6" }, 6)]
		[TestCase(new[] { "2d4" }, 4)]
		[TestCase(new[] { "2d6" }, 6)]
		[TestCase(new[] { "6d4" }, 4)]
		[TestCase(new[] { "-6d4" }, 4)]
		public void ConvertInputToDiceRoll_ReturnsCorrectDieSizeWhenDiceExpressionParserReturnsSingleExpression(string[] diceExpressions, int expectedDieSize)
		{
			diceExpressionParser.Setup(parser => parser.Parse(It.IsAny<string>())).Returns(diceExpressions);

			DiceRoll actual = inputDiceRollConverter.ConvertInputToDiceRoll("");
			int actualDieSize = GatherDieSizesFrom(actual)[0];

			Assert.AreEqual(expectedDieSize, actualDieSize);
		}

		[TestCase(new[] { "1d4", "1d4" }, 4, 4)]
		[TestCase(new[] { "1d6", "1d8" }, 6, 8)]
		[TestCase(new[] { "1d8", "1d6" }, 8, 6)]
		[TestCase(new[] { "2d4", "3d6" }, 4, 6)]
		[TestCase(new[] { "3d4", "2d6" }, 4, 6)]
		[TestCase(new[] { "1d4", "1d6", "1d8" }, 4, 6, 8)]
		[TestCase(new[] { "1d8", "1d4", "1d6" }, 8, 4, 6)]
		[TestCase(new[] { "-1d4", "-1d6", "-1d8" }, 4, 6, 8)]
		public void ConvertInputToDiceRoll_ReturnsCorrectDieSizeWhenDiceExpressionParserReturnsMultipleExpressions(string[] diceExpressions,
			params int[] expectedDieSize)
		{
			diceExpressionParser.Setup(parser => parser.Parse(It.IsAny<string>())).Returns(diceExpressions);

			DiceRoll actual = inputDiceRollConverter.ConvertInputToDiceRoll("");
			int[] actualDieSize = GatherDieSizesFrom(actual);

			Assert.AreEqual(expectedDieSize, actualDieSize);
		}

		[TestCase(new string[0], new int[0])]
		[TestCase(new[] { "1" }, 1)]
		[TestCase(new[] { "2" }, 2)]
		[TestCase(new[] { "-1" }, -1)]
		[TestCase(new[] { "-2" }, -2)]
		[TestCase(new[] { "-100" }, -100)]
		public void ConvertInputToDiceRoll_ReturnsCorrectModifiersWhenModifierExpressionParserReturnsSingleExpression(string[] modifierExpressions,
			params int[] expectedModifiers)
		{
			modifierExpressionParser.Setup(parser => parser.Parse(It.IsAny<string>())).Returns(modifierExpressions);

			DiceRoll actual = inputDiceRollConverter.ConvertInputToDiceRoll("");

			Assert.AreEqual(expectedModifiers, actual.ModifierExpressions);
		}

		[TestCase(new[] { "1", "1" }, 1, 1)]
		[TestCase(new[] { "1", "2" }, 1, 2)]
		[TestCase(new[] { "2", "1" }, 2, 1)]
		[TestCase(new[] { "1", "-1" }, 1, -1)]
		[TestCase(new[] { "-1", "1" }, -1, 1)]
		[TestCase(new[] { "-1", "-1" }, -1, -1)]
		[TestCase(new[] { "1", "-2" }, 1, -2)]
		[TestCase(new[] { "1", "2", "4" }, 1, 2, 4)]
		[TestCase(new[] { "-1", "2", "4" }, -1, 2, 4)]
		[TestCase(new[] { "1", "-2", "4" }, 1, -2, 4)]
		[TestCase(new[] { "1", "2", "-4" }, 1, 2, -4)]
		[TestCase(new[] { "-1", "-2", "-4" }, -1, -2, -4)]
		[TestCase(new[] { "1", "2", "4", "8" }, 1, 2, 4, 8)]
		[TestCase(new[] { "-1", "-2", "-4", "-8" }, -1, -2, -4, -8)]
		public void ConvertInputToDiceRoll_ReturnsCorrectModifiersWhenModifierExpressionParserReturnsMultipleExpressions(string[] modifierExpressions,
			params int[] expectedModifiers)
		{
			modifierExpressionParser.Setup(parser => parser.Parse(It.IsAny<string>())).Returns(modifierExpressions);

			DiceRoll actual = inputDiceRollConverter.ConvertInputToDiceRoll("");

			Assert.AreEqual(expectedModifiers, actual.ModifierExpressions);
		}

		private static int[] GatherDiceAmountsFrom(DiceRoll diceRoll)
		{
			return diceRoll.DiceExpressions.Select(expression => expression.diceAmount).ToArray();
		}

		private static int[] GatherDieSizesFrom(DiceRoll diceRoll)
		{
			return diceRoll.DiceExpressions.Select(expression => expression.dieSize).ToArray();
		}
	}
}
namespace DnDRulesetTests.Model.Services.Parsers
{
	using DnDRuleset.Model.Exceptions;
	using DnDRuleset.Model.Services.Parsers;

	using NUnit.Framework;

	[TestFixture]
	public class ModifierExpressionParserTests
	{
		private ModifierExpressionParser modifierExpressionParser;

		[SetUp]
		public void SetUp()
		{
			modifierExpressionParser = new ModifierExpressionParser();
		}

		[TestCase("1d4")]
		[TestCase("1", "1")]
		[TestCase("2", "2")]
		[TestCase("100", "100")]
		[TestCase("+1", "1")]
		[TestCase("-1", "-1")]
		public void Parse_ReturnsCorrectModifierExpressionsWhenGivenSingleModifierRollInput(string input, params string[] expectedOutput)
		{
			string[] actual = modifierExpressionParser.Parse(input);

			Assert.AreEqual(expectedOutput, actual);
		}

		[TestCase(" 1", "1")]
		[TestCase(" 2", "2")]
		[TestCase("1 ", "1")]
		[TestCase("2 ", "2")]
		[TestCase(" 1 ", "1")]
		[TestCase(" 2 ", "2")]
		[TestCase("  1  ", "1")]
		[TestCase("  2  ", "2")]
		public void Parse_ReturnsCorrectModifierExpressionsWhenGivenSingleModifierRollInputWithSpaces(string input, params string[] expectedOutput)
		{
			string[] actual = modifierExpressionParser.Parse(input);

			Assert.AreEqual(expectedOutput, actual);
		}

		[TestCase("1d4+1", "1")]
		[TestCase("1d4 +1", "1")]
		[TestCase("1d4 + 1", "1")]
		[TestCase("1d4+ 1", "1")]
		[TestCase("1+1d4", "1")]
		[TestCase("1d4+2", "2")]
		[TestCase("2+1d4", "2")]
		[TestCase("1d20+1", "1")]
		[TestCase("20d4+1", "1")]
		[TestCase("1+1d20", "1")]
		[TestCase("1+20d4", "1")]
		[TestCase("1d4-1", "-1")]
		[TestCase("1-1d4", "1")]
		[TestCase("1d4-2", "-2")]
		[TestCase("2-1d4", "2")]
		[TestCase("1d20-1", "-1")]
		[TestCase("20d4-1", "-1")]
		[TestCase("1-1d20", "1")]
		[TestCase("1-20d4", "1")]
		public void Parse_ReturnsCorrectModifierExpressionsWhenGivenSingleModifierRollInputWithNonModifierExpressions(string input, params string[] expectedOutput)
		{
			string[] actual = modifierExpressionParser.Parse(input);

			Assert.AreEqual(expectedOutput, actual);
		}

		[TestCase("1+1", "1", "1")]
		[TestCase("1+2", "1", "2")]
		[TestCase("2+1", "2", "1")]
		[TestCase("1-1", "1", "-1")]
		[TestCase("-1+2", "-1", "2")]
		[TestCase("-1-2", "-1", "-2")]
		[TestCase("1-2", "1", "-2")]
		[TestCase("2-1", "2", "-1")]
		[TestCase("1+2+3", "1", "2", "3")]
		public void Parse_ReturnsCorrectModifierExpressionsWhenGivenMultipleModifierRollInputs(string input, params string[] expectedOutput)
		{
			string[] actual = modifierExpressionParser.Parse(input);

			Assert.AreEqual(expectedOutput, actual);
		}

		[TestCase("1+3d4+2", "1", "2")]
		[TestCase("2+3d4+1", "2", "1")]
		[TestCase("1-3d4+2", "1", "2")]
		[TestCase("1+3d4-2", "1", "-2")]
		[TestCase("1-3d4-2", "1", "-2")]
		[TestCase("-1-3d4-2", "-1", "-2")]
		[TestCase("-1+3d4+2", "-1", "2")]
		[TestCase("1+3d4+5d6+2", "1", "2")]
		[TestCase("3d4+1+5d6+2", "1", "2")]
		[TestCase("3d4+2+5d6+1", "2", "1")]
		[TestCase("-3d4+1-5d6+2", "1", "2")]
		[TestCase("3d4-1+5d6+2", "-1", "2")]
		[TestCase("3d4+1+5d6-2", "1", "-2")]
		[TestCase("3d4-1+5d6-2", "-1", "-2")]
		[TestCase("-3d4-1-5d6-2", "-1", "-2")]
		[TestCase("-3d4+1-5d6+2", "1", "2")]
		[TestCase("1+1d4+2+3d6+3+4d8+4+1d4+10", "1", "2", "3", "4", "10")]
		public void Parse_ReturnsCorrectModifierExpressionsWhenGivenMultipleModifierRollInputsWithNonModifierExpressions(string input, params string[] expectedOutput)
		{
			string[] actual = modifierExpressionParser.Parse(input);

			Assert.AreEqual(expectedOutput, actual);
		}

		[TestCase("1h")]
		[TestCase("h1")]
		[TestCase("+1h")]
		[TestCase("+h1")]
		[TestCase("-1h")]
		[TestCase("h1h")]
		[TestCase("1d20+1!20")]
		[TestCase("1d20-1!20")]
		[TestCase("1h20")]
		[TestCase("1d20+1h20+1d20")]
		public void Parse_ThrowsRollParsingExceptionWhenGivenRollInputsWithUnrecognizedExpressions(string input)
		{
			RollParsingException exception = Assert.Throws<RollParsingException>(() => modifierExpressionParser.Parse(input));

			Assert.AreEqual("Unable to parse roll input: " + input, exception?.Message);
		}
	}
}
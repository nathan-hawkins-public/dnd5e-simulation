namespace DnDRulesetTests.Model.Services.Parsers
{
	using DnDRuleset.Model.Exceptions;
	using DnDRuleset.Model.Services.Parsers;

	using NUnit.Framework;

	[TestFixture]
	public class DiceExpressionParserTests
	{
		private DiceExpressionParser diceExpressionParser;

		[SetUp]
		public void SetUp()
		{
			diceExpressionParser = new DiceExpressionParser();
		}

		[TestCase("1")]
		[TestCase("d4", "1d4")]
		[TestCase("1d4", "1d4")]
		[TestCase("1d6", "1d6")]
		[TestCase("1d8", "1d8")]
		[TestCase("1d10", "1d10")]
		[TestCase("1d12", "1d12")]
		[TestCase("1d20", "1d20")]
		[TestCase("1d100", "1d100")]
		[TestCase("2d20", "2d20")]
		[TestCase("100d20", "100d20")]
		public void Parse_ReturnsCorrectDiceExpressionsWhenGivenSingleStandardRollInput(string input, params string[] expectedOutput)
		{
			string[] actual = diceExpressionParser.Parse(input);

			Assert.AreEqual(expectedOutput, actual);
		}

		[TestCase("+1d20", "1d20")]
		[TestCase("-1d20", "-1d20")]
		public void Parse_ReturnsCorrectDiceExpressionsWhenGivenSingleRollInputWithOperators(string input, params string[] expectedOutput)
		{
			string[] actual = diceExpressionParser.Parse(input);

			Assert.AreEqual(expectedOutput, actual);
		}

		[TestCase(" 1d20", "1d20")]
		[TestCase(" 1 d20", "1d20")]
		[TestCase("1 d20", "1d20")]
		[TestCase("  1  d20", "1d20")]
		[TestCase("1d 20", "1d20")]
		[TestCase("1d 20 ", "1d20")]
		[TestCase("1d20 ", "1d20")]
		[TestCase("1d  20  ", "1d20")]
		public void Parse_ReturnsCorrectDiceExpressionsWhenGivenSingleRollInputWithSpaces(string input, params string[] expectedOutput)
		{
			string[] actual = diceExpressionParser.Parse(input);

			Assert.AreEqual(expectedOutput, actual);
		}

		[TestCase("1d20+1", "1d20")]
		[TestCase("1d20 +1", "1d20")]
		[TestCase("1d20 + 1", "1d20")]
		[TestCase("1d20+ 1", "1d20")]
		[TestCase("2d4+1", "2d4")]
		[TestCase("2d4+1+5", "2d4")]
		[TestCase("1+2d4+5", "2d4")]
		[TestCase("-1+2d4+5", "2d4")]
		[TestCase("1-2d4+5", "-2d4")]
		[TestCase("-1-2d4+5", "-2d4")]
		[TestCase("1+2d4-5", "2d4")]
		[TestCase("1-2d4-5", "-2d4")]
		[TestCase("-1-2d4-5", "-2d4")]
		[TestCase("-1+2d4-5", "2d4")]
		public void Parse_ReturnsCorrectDiceExpressionsWhenGivenSingleRollInputWithNonDiceExpressions(string input, params string[] expectedOutput)
		{
			string[] actual = diceExpressionParser.Parse(input);

			Assert.AreEqual(expectedOutput, actual);
		}

		[TestCase("1d4+1d4", "1d4", "1d4")]
		[TestCase("2d4+3d20", "2d4", "3d20")]
		[TestCase("5d4+3d20", "5d4", "3d20")]
		[TestCase("2d20+3d4", "2d20", "3d4")]
		[TestCase("1d4+1d4+1d20", "1d4", "1d4", "1d20")]
		[TestCase("1d4+1d6+1d8+1d10+1d12+1d20+1d100", "1d4", "1d6", "1d8", "1d10", "1d12", "1d20", "1d100")]
		[TestCase("1d4-1d6", "1d4", "-1d6")]
		[TestCase("1d4-2d6", "1d4", "-2d6")]
		[TestCase("1d4-1d6", "1d4", "-1d6")]
		[TestCase("-1d4+1d6", "-1d4", "1d6")]
		[TestCase("-1d4-1d6", "-1d4", "-1d6")]
		[TestCase("1d4-1d6-1d8-1d10-1d12-1d20-1d100", "1d4", "-1d6", "-1d8", "-1d10", "-1d12", "-1d20", "-1d100")]
		public void Parse_ReturnsCorrectDiceExpressionsWhenGivenMultipleRollInputsWithOperators(string input, params string[] expectedOutput)
		{
			string[] actual = diceExpressionParser.Parse(input);

			Assert.AreEqual(expectedOutput, actual);
		}

		[TestCase("d4+d6+1", "1d4", "1d6")]
		[TestCase("1d4+1d6+1", "1d4", "1d6")]
		[TestCase("1d4+1d6+1+1", "1d4", "1d6")]
		[TestCase("2d6+3d4+1", "2d6", "3d4")]
		[TestCase("5+2d6+3d4+1", "2d6", "3d4")]
		[TestCase("1+2d4+3d6+5", "2d4", "3d6")]
		[TestCase("1-2d4+3d6+5", "-2d4", "3d6")]
		[TestCase("1-2d4-3d6+5", "-2d4", "-3d6")]
		[TestCase("-1+2d4+3d6+5", "2d4", "3d6")]
		[TestCase("-1+2d4+3d6-5", "2d4", "3d6")]
		[TestCase("-1-2d4+3d6-5", "-2d4", "3d6")]
		[TestCase("-1+2d4-3d6-5", "2d4", "-3d6")]
		[TestCase("-1-2d4-3d6-5", "-2d4", "-3d6")]
		[TestCase("1+1d4+2+3d6+3+4d8+4+1d4+10", "1d4", "3d6", "4d8", "1d4")]
		[TestCase("1+1d4+2+3d6+3+d8+4+1d4+10", "1d4", "3d6", "1d8", "1d4")]
		[TestCase("1+1d4+2+3d6+3-d8+4+1d4+10", "1d4", "3d6", "-1d8", "1d4")]
		[TestCase("1+1d4+2-3d6+3+4d8+4-1d4+10", "1d4", "-3d6", "4d8", "-1d4")]
		public void Parse_ReturnsCorrectDiceExpressionsWhenGivenMultipleRollInputsWithNonDiceExpressions(string input, params string[] expectedOutput)
		{
			string[] actual = diceExpressionParser.Parse(input);

			Assert.AreEqual(expectedOutput, actual);
		}

		[TestCase("1d20!")]
		[TestCase("1d!20")]
		[TestCase("1d2s")]
		[TestCase("1dhi")]
		[TestCase("dd20")]
		[TestCase("!1d20")]
		[TestCase("1!d20")]
		[TestCase("1d20+1!d20")]
		[TestCase("1d20-1!d20")]
		[TestCase("1d20+1dh20+1d20")]
		public void Parse_ThrowsRollParsingExceptionWhenGivenRollInputsWithUnrecognizedExpressions(string input)
		{
			RollParsingException exception = Assert.Throws<RollParsingException>(() => diceExpressionParser.Parse(input));

			Assert.AreEqual("Unable to parse roll input: " + input, exception?.Message);
		}
	}
}
namespace DnDRulesetTests.Model.Domain
{
	using System;
	using System.Collections.Generic;

	using DnDRuleset.Model.Domain;
	using DnDRuleset.Model.Services;

	using DnDRulesetTests.Utils;

	using Moq;

	using NUnit.Framework;

	[TestFixture]
	public class DiceRollTests
	{
		private Mock<RandomNumberGenerator> randomNumberGenerator;
		private DiceRoll diceRoll;

		[SetUp]
		public void SetUp()
		{
			randomNumberGenerator = new Mock<RandomNumberGenerator>();
			diceRoll = new DiceRoll(randomNumberGenerator.Object, new List<(int, int)>(), new List<int>());
		}

		[TestCase(1, 4)]
		[TestCase(1, 6)]
		[TestCase(2, 4)]
		[TestCase(4, 8)]
		[TestCase(8, 6)]
		[TestCase(-1, 4)]
		[TestCase(-1, 6)]
		[TestCase(-2, 4)]
		[TestCase(-4, 8)]
		[TestCase(-8, 6)]
		public void Roll_CallsRandomNumberGeneratorCorrectlyWhenSingleExpressionExists(int diceAmount, int dieSize)
		{
			diceRoll.DiceExpressions.Add((diceAmount, dieSize));

			diceRoll.Roll();

			randomNumberGenerator.Verify(generator => generator.Next(dieSize), Times.Exactly(Math.Abs(diceAmount)));
		}

		[TestCase(1, 4, 1, 6)]
		[TestCase(2, 4, 3, 6)]
		[TestCase(-1, 4, -1, 6)]
		[TestCase(-2, 4, -3, 6)]
		public void Roll_CallsRandomNumberGeneratorCorrectlyWhenMultipleExpressionsExist(int diceAmount1, int dieSize1, int diceAmount2, int dieSize2)
		{
			diceRoll.DiceExpressions.Add((diceAmount1, dieSize1));
			diceRoll.DiceExpressions.Add((diceAmount2, dieSize2));

			diceRoll.Roll();

			randomNumberGenerator.Verify(generator => generator.Next(dieSize1), Times.Exactly(Math.Abs(diceAmount1)));
			randomNumberGenerator.Verify(generator => generator.Next(dieSize2), Times.Exactly(Math.Abs(diceAmount2)));
			randomNumberGenerator.Verify(generator => generator.Next(It.IsAny<int>()), Times.Exactly(Math.Abs(diceAmount1) + Math.Abs(diceAmount2)));
		}

		private static IEnumerable<TestCaseData> ReturnsDiceAmountTimesForWhatRandomNumberGeneratorReturnsWhenSingleExpressionExistsData
		{
			get
			{
				yield return new TestCaseData(new[] { 1, 2 }, 3);
				yield return new TestCaseData(new[] { 1, 2, 3 }, 6);
				yield return new TestCaseData(new[] { 3, 2, 1 }, 6);
				yield return new TestCaseData(new[] { 1, 2, 100 }, 103);
			}
		}

		[TestCaseSource(nameof(ReturnsDiceAmountTimesForWhatRandomNumberGeneratorReturnsWhenSingleExpressionExistsData))]
		public void Roll_ReturnsDiceAmountTimesForWhatRandomNumberGeneratorReturnsWhenSingleExpressionExists(int[] rolls, int expected)
		{
			randomNumberGenerator.Setup(generator => generator.Next(It.IsAny<int>())).ReturnsInOrder(rolls);

			diceRoll.DiceExpressions.Add((rolls.Length, 0));

			Assert.AreEqual(expected, diceRoll.Roll());
		}

		private static IEnumerable<TestCaseData> ReturnsDiceAmountTimesWithinDieSizeRangeWhenSingleExpressionExistsData
		{
			get
			{
				yield return new TestCaseData(1, 4, 4);
				yield return new TestCaseData(-1, 4, -4);
				yield return new TestCaseData(2, 4, 7);
				yield return new TestCaseData(4, 4, 9);
				yield return new TestCaseData(20, 4, 25);
				yield return new TestCaseData(1, 6, 6);
				yield return new TestCaseData(2, 6, 11);
				yield return new TestCaseData(1, 20, 20);
				yield return new TestCaseData(2, 20, 39);
				yield return new TestCaseData(3, 20, 40);
			}
		}

		[TestCaseSource(nameof(ReturnsDiceAmountTimesWithinDieSizeRangeWhenSingleExpressionExistsData))]
		public void Roll_ReturnsDiceAmountTimesWithinDieSizeRangeWhenSingleExpressionExists(int diceAmount, int dieSize, int expected)
		{
			randomNumberGenerator.SetupSequence(generator => generator.Next(It.IsAny<int>()))
				.Returns(dieSize)
				.Returns(dieSize - 1)
				.ReturnsForever(1);

			diceRoll.DiceExpressions.Add((diceAmount, dieSize));

			Assert.AreEqual(expected, diceRoll.Roll());
		}

		private static IEnumerable<TestCaseData> ReturnsDiceAmountTimesWithinDieSizeRangesWhenMultipleExpressionsExists
		{
			get
			{
				yield return new TestCaseData(new[] { (1, 4), (1, 6) }, 10);
				yield return new TestCaseData(new[] { (2, 4), (1, 6) }, 13);
				yield return new TestCaseData(new[] { (4, 4), (1, 6) }, 15);
				yield return new TestCaseData(new[] { (1, 4), (2, 6) }, 15);
				yield return new TestCaseData(new[] { (1, 4), (3, 6) }, 16);
				yield return new TestCaseData(new[] { (1, 4), (6, 6) }, 19);
				yield return new TestCaseData(new[] { (2, 4), (2, 6) }, 18);
				yield return new TestCaseData(new[] { (20, 4), (20, 6) }, 54);
				yield return new TestCaseData(new[] { (1, 4), (-1, 6) }, -2);
				yield return new TestCaseData(new[] { (2, 4), (-1, 6) }, 1);
				yield return new TestCaseData(new[] { (1, 20), (-1, 4) }, 16);
				yield return new TestCaseData(new[] { (1, 20), (-1, 4), (-1, 6) }, 10);
				yield return new TestCaseData(new[] { (1, 20), (-3, 4) }, 12);
			}
		}

		[TestCaseSource(nameof(ReturnsDiceAmountTimesWithinDieSizeRangesWhenMultipleExpressionsExists))]
		public void Roll_ReturnsDiceAmountTimesWithinDieSizeRangesWhenMultipleExpressionsExists((int diceAmount, int dieSize)[] diceExpressions, int expected)
		{
			foreach ((int _, int dieSize) in diceExpressions)
			{
				randomNumberGenerator.SetupSequence(generator => generator.Next(dieSize))
					.Returns(dieSize)
					.Returns(dieSize - 1)
					.ReturnsForever(1);
			}

			diceRoll.DiceExpressions.AddRange(diceExpressions);

			Assert.AreEqual(expected, diceRoll.Roll());
		}

		[TestCase(1)]
		[TestCase(2)]
		[TestCase(-1)]
		public void Roll_ReturnsModifierTotalWhenSingleModifierExists(int modifier)
		{
			diceRoll.ModifierExpressions.Add(modifier);

			Assert.AreEqual(modifier, diceRoll.Roll());
		}

		[TestCase(new[] { 1, 2 }, 3)]
		[TestCase(new[] { 2, 1 }, 3)]
		[TestCase(new[] { 1, -2 }, -1)]
		[TestCase(new[] { -1, 2 }, 1)]
		[TestCase(new[] { 1, 2, 3 }, 6)]
		[TestCase(new[] { 1, 2, -3 }, 0)]
		[TestCase(new[] { 1, 2, 3, 4, 5, 100 }, 115)]
		[TestCase(new[] { -1, 2, 3, 4, -5, -100 }, -97)]
		public void Roll_ReturnsModifierTotalWhenMultipleModifiersExists(int[] modifiers, int expected)
		{
			diceRoll.ModifierExpressions.AddRange(modifiers);

			Assert.AreEqual(expected, diceRoll.Roll());
		}
	}
}
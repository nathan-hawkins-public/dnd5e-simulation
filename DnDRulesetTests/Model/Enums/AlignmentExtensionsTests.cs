using NUnit.Framework;

namespace DnDRulesetTests.Model.Enums
{
	using DnDRuleset.Model.Enums;

	[TestFixture]
	public class AlignmentExtensionsTests
	{
		[TestCase(Alignment.LawfulGood)]
		[TestCase(Alignment.LawfulNeutral)]
		[TestCase(Alignment.LawfulEvil)]
		public void IsLawful_ReturnsTrueWhenAlignmentIsGood(Alignment alignment)
		{
			Assert.True(alignment.IsLawful());
		}

		[TestCase(Alignment.NeutralGood)]
		[TestCase(Alignment.ChaoticGood)]
		[TestCase(Alignment.TrueNeutral)]
		[TestCase(Alignment.ChaoticNeutral)]
		[TestCase(Alignment.NeutralEvil)]
		[TestCase(Alignment.ChaoticEvil)]
		public void IsLawful_ReturnsFalseWhenAlignmentIsNotGood(Alignment alignment)
		{
			Assert.False(alignment.IsLawful());
		}

		[TestCase(Alignment.NeutralGood)]
		[TestCase(Alignment.LawfulNeutral)]
		[TestCase(Alignment.TrueNeutral)]
		[TestCase(Alignment.ChaoticNeutral)]
		[TestCase(Alignment.NeutralEvil)]
		public void IsNeutral_ReturnsTrueWhenAlignmentIsNeutral(Alignment alignment)
		{
			Assert.True(alignment.IsNeutral());
		}

		[TestCase(Alignment.LawfulGood)]
		[TestCase(Alignment.ChaoticGood)]
		[TestCase(Alignment.LawfulEvil)]
		[TestCase(Alignment.ChaoticEvil)]
		public void IsNeutral_ReturnsFalseWhenAlignmentIsNotNeutral(Alignment alignment)
		{
			Assert.False(alignment.IsNeutral());
		}

		[TestCase(Alignment.ChaoticGood)]
		[TestCase(Alignment.ChaoticNeutral)]
		[TestCase(Alignment.ChaoticEvil)]
		public void IsChaotic_ReturnsTrueWhenAlignmentIsChaotic(Alignment alignment)
		{
			Assert.True(alignment.IsChaotic());
		}

		[TestCase(Alignment.LawfulGood)]
		[TestCase(Alignment.NeutralGood)]
		[TestCase(Alignment.LawfulNeutral)]
		[TestCase(Alignment.TrueNeutral)]
		[TestCase(Alignment.LawfulEvil)]
		[TestCase(Alignment.NeutralEvil)]
		public void IsChaotic_ReturnsFalseWhenAlignmentIsNotChaotic(Alignment alignment)
		{
			Assert.False(alignment.IsChaotic());
		}

		[TestCase(Alignment.LawfulGood)]
		[TestCase(Alignment.NeutralGood)]
		[TestCase(Alignment.ChaoticGood)]
		public void IsGood_ReturnsTrueWhenAlignmentIsGood(Alignment alignment)
		{
			Assert.True(alignment.IsGood());
		}
		
		[TestCase(Alignment.LawfulNeutral)]
		[TestCase(Alignment.TrueNeutral)]
		[TestCase(Alignment.ChaoticNeutral)]
		[TestCase(Alignment.LawfulEvil)]
		[TestCase(Alignment.NeutralEvil)]
		[TestCase(Alignment.ChaoticEvil)]
		public void IsGood_ReturnsFalseWhenAlignmentIsNotGood(Alignment alignment)
		{
			Assert.False(alignment.IsGood());
		}
		
		[TestCase(Alignment.LawfulEvil)]
		[TestCase(Alignment.NeutralEvil)]
		[TestCase(Alignment.ChaoticEvil)]
		public void IsEvil_ReturnsTrueWhenAlignmentIsEvil(Alignment alignment)
		{
			Assert.True(alignment.IsEvil());
		}
		
		[TestCase(Alignment.LawfulGood)]
		[TestCase(Alignment.NeutralGood)]
		[TestCase(Alignment.ChaoticGood)]
		[TestCase(Alignment.LawfulNeutral)]
		[TestCase(Alignment.TrueNeutral)]
		[TestCase(Alignment.ChaoticNeutral)]
		public void IsEvil_ReturnsFalseWhenAlignmentIsNotEvil(Alignment alignment)
		{
			Assert.False(alignment.IsEvil());
		}
	}
}
namespace DnDRulesetTests.Utils
{
	using System.Collections.Generic;

	using Moq.Language;
	using Moq.Language.Flow;

	public static class MoqExtensions
	{
		public static void ReturnsInOrder<T, TResult>(this ISetup<T, TResult> setup, params TResult[] results) where T : class
		{
			setup.Returns(new Queue<TResult>(results).Dequeue);
		}

		public static void ReturnsForever<T, TResult>(this ISetup<T, TResult> setup, TResult result) where T : class
		{
			setup.Returns(() =>
				{
					setup.ReturnsForever(result);
					return result;
				});
		}

		public static void ReturnsForever<TResult>(this ISetupSequentialResult<TResult> setup, TResult result)
		{
			setup.Returns(() =>
				{
					setup.ReturnsForever(result);
					return result;
				});
		}
	}
}